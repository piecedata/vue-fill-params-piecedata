import Regexp from 'path-to-regexp';

const regexpCompileCache = [];

export function fillParams(path, params) {
    try {
        const filler = regexpCompileCache[path]
            || (regexpCompileCache[path] = Regexp.compile(path));
        return filler(params || {}, {
            pretty: true,
        });
    } catch (e) {
        if (process.env.NODE_ENV !== 'production') {
            console.warn(`missing param for ${path}: ${e.message}`);
        }

        return '';
    }
}
